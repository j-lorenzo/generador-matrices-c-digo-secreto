from flask import Flask, render_template
from modules.game_manager import GameManager

app = Flask('generacodigo')
@app.route('/')
def index():
    game_manager = GameManager()
    matrix = game_manager.get_matrix()
    whos_first = game_manager.whos_first()
    return render_template('index.html', first_team=whos_first, matrix=matrix)

if __name__ == '__main__':
    app.run()
