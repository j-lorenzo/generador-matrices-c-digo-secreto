from random import shuffle

class MatrixManager:

    ### Generate the color sequence ordered
    def gen_color_ordered_sequence(self, color_tiles):
        color_ordered_sequence = []
        for color in color_tiles:
            number = 0
            while number < color_tiles[color]:
                color_ordered_sequence.append(color)
                number = number+1
        return color_ordered_sequence

    ### Shuffle the color sequence
    def shuffle_sequence(self, array):
        shuffled_sequence = array
        shuffle(shuffled_sequence)
        return shuffled_sequence

    ### ChunkIt 
    # https://stackoverflow.com/questions/2130016/splitting-a-list-into-n-parts-of-approximately-equal-length
    def chunk_it(self, seq, num):
        avg = len(seq) / float(num)
        out = []
        last = 0.0
        while last < len(seq):
            out.append(seq[int(last):int(last + avg)])
            last += avg
        return out


